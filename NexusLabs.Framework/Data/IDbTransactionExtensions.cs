﻿using NexusLabs.Framework;

namespace System.Data
{
    public static class IDbTransactionExtensions
    {
        public static TriedEx<bool> TryRollback(
            this IDbTransaction transaction) =>
            Safely.GetResultOrException(() =>
            {
                if (transaction.Connection != null &&
                    transaction.Connection.State != ConnectionState.Closed)
                {
                    transaction.Rollback();
                }

                return true;
            });
    }
}